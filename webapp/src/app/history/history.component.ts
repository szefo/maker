// Copyright (C) 2018 Cranky Kernel
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

import {Component, OnInit} from '@angular/core';
import {TradeState} from '../maker.service';
import {toAppTradeState} from '../trade-table/trade-table.component';
import {MakerApiService} from "../maker-api.service";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
    selector: 'app-history',
    templateUrl: './history.component.html',
    styleUrls: ['./history.component.scss']
})
export class HistoryComponent implements OnInit {

    trades: TradeState[] = [];

    page: number = 0;

    pageSize = 50;

    constructor(private makerApi: MakerApiService,
                private route: ActivatedRoute,
                private router: Router) {
    }

    ngOnInit() {
        this.route.params.subscribe((params) => {
            console.log(params);
            if (params.page !== undefined) {
                this.page = +params.page;
            }
            this.refresh()
        });
    }

    refresh() {
        let offset = this.page * this.pageSize;
        console.log(offset);
        this.makerApi.get("/api/trade/query", {
            params: {
                offset: offset,
                limit: this.pageSize,
            }
        }).subscribe((trades: TradeState[]) => {
            this.trades = trades
                .map((trade) => {
                    return toAppTradeState(trade);
                });
        });
    }

    gotoPreviousPage() {
        let page = this.page < 1 ? 0 : this.page - 1;
        let params = {
            page: page,
        };
        this.router.navigate([".", params], {
            queryParamsHandling: "merge",
        });
    }

    gotoNextPage() {
        let params = {
            page: this.page + 1,
        };
        this.router.navigate([".", params], {
            queryParamsHandling: "merge",
        });
    }
}
